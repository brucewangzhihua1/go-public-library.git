package file

import (
	"strings"

	"gitee.com/brucewangzhihua1/go-public-library/config/encoder"
)

func format(p string, e encoder.Encoder) string {
	parts := strings.Split(p, ".")
	if len(parts) > 1 {
		return parts[len(parts)-1]
	}
	return e.String()
}
